import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    FlatList
 } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import VideoItem from './Video/videoItem';
import data from './images/data.json';

export default class app extends React.Component {
    render() {
        return (
            < View style={styles.container}>
                <View style={styles.navBar}>
                    <Image source={require('./images/logo.png')} style={{width: 100, height: 22}}/>
                        <View style={styles.rightNav}> 
                            <TouchableOpacity>
                                <Icon style={styles.navItem} name='search' size={25} />
                                </TouchableOpacity>
                            <TouchableOpacity>
                                <Icon style={styles.navItem} name='account-circle' size={25} />
                                </TouchableOpacity>
                        </View>
                </View>
            <View style={styles.Body}>
            <FlatList 
            data={data.items}
            renderItem={(video)=><VideoItem video= {video.item}/>}
            keyExtractor= {(item)=>item.id}
            itemSeperatorComponent={()=><View style={{height:0.5,backgroundColor: '#E5E5E5'}}/>}
            />
            </View>
            <View style={styles.tabBar}>
            <TouchableOpacity style={styles.tabItem}>
                <Icon name='home' size={25} />
                <Text style={styles.tabTitle}>Home</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.tabItem}>
                <Icon name='whatshot' size={25} />
                <Text style={styles.tabTitle}>Trending</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.tabItem}>
                <Icon name='subscriptions' size={25} />
                <Text style={styles.tabTitle}>Subscriptions</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.tabItem}>
                <Icon name='folder' size={25} />
                <Text style={styles.tabTitle}>Lirary</Text>
            </TouchableOpacity>
            </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,

    },
    navBar:{
        height :60,
        backgroundColor : 'white',
        elevation : 3,
        paddingHorizontal: 20,
        marginTop : 30,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    rightNav:{
        flexDirection:'row'

    },
    navItem:{
        marginLeft: 25
    },
    Body:{
        flex:1
    },
    tabBar:{
        height:60,
        backgroundColor:'white',
        borderTopWidth: 0.5,
        borderColor: '#E5E5E5',
        flexDirection:'row',
        justifyContent: 'space-around'
    },
    tabItem:{
        alignItems: 'center',
        justifyContent: 'center'
    },
    tabTitle:{
        fontSize:11,
        color: '#3c3c3c',
        paddingTop: 4
    }
})