import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
 } from 'react-native';
 import Icon from 'react-native-vector-icons/'

 export default class LoginScreen extends React.Component{
     render() {
         return(
         <View style={styles.container}>
             <View style={styles.Top}>
             <Image source={require('./images/logo.png')} style={styles.imagestyle}/>
             <Text style={styles.textstyle}>Register</Text>
             </View>
            <View style={styles.body}>
                <Text style={styles.textbody}>Username</Text>
                <View style={styles.writingspace}/>
                <Text style={styles.textbody}>Email</Text>
                <View style={styles.writingspace}/>
                <Text style={styles.textbody}>Password</Text>
                <View style={styles.writingspace}/>
                <Text style={styles.textbody}>Ulangi Password</Text>
                <View style={styles.writingspace}/>
            </View>
            <View style={styles.button}>
                <TouchableOpacity>
                    <View style={styles.buttonstyle}>
                        <Text style={styles.buttondesc}>Daftar</Text>
                    </View>
                </TouchableOpacity>
                <Text style={styles.buttontext}>atau</Text>
                <TouchableOpacity>
                    <View style={styles.buttonstyle2}>
                        <Text style={styles.buttondesc}>Masuk</Text>
                    </View>
                </TouchableOpacity>
            </View>
         </View>
         )
     }
     
 }

 const styles = StyleSheet.create({
     container:{
         flex:1,
     },
     Top:{
         marginTop:15,
         paddingTop:40,
        justifyContent:'center',
        alignItems:'center'
     },
     textstyle:{
         paddingTop:30,
         fontSize:24,
         color: '#003366'
     },
     body:{
         flex:1,
         paddingTop:20,
         alignItems:'flex-start',
         marginLeft:40,
         marginRight:41,
         justifyContent:'space-evenly'
     },
     textbody:{
         fontSize:16,
         color:'#003366'
     },
     writingspace:{
         height:48,
         width:294,
         borderColor:'#003366',
         borderWidth:1
     },
     button:{
         height:177,
         padding:20,
         justifyContent:'space-around',
         alignItems:'center'
     },
     buttonstyle:{
         height:40,
         width:140,
         backgroundColor:'#003366',
         borderRadius:16
     },
     buttonstyle2:{
        height:40,
        width:140,
        backgroundColor:'#3EC6FF',
        borderRadius:16
    },
     buttontext:{
         fontSize:24,
         color:'#3EC5FF',
         textAlign:'center'
     },
     buttondesc:{
         fontSize:24,
         color:'#FFFFFF',
         textAlign:'center',
         textAlignVertical:'center'
         
     }
        

 })