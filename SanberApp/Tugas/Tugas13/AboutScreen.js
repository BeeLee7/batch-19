import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    FlatList
 } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
 import { FontAwesome } from '@expo/vector-icons';
 import { AntDesign } from '@expo/vector-icons';
export default class AboutScreen extends React.Component {
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.page2}>
                <Text style={styles.text1}>Tentang Saya</Text>
                <Icon style={styles.iconstyle} name='account-circle' size={200}/>
                <Text style={styles.text2}>Ho-oh</Text>
                <Text style={styles.text3}>React Native Developer</Text>
                </View>
                <View style={styles.Box1}>
                    <Text style={styles.text4}>Portofolio</Text>
                    <View style={styles.minibox}>
                    <View style={styles.tampilan}>
                    <FontAwesome style={styles.iconstyle2} name='gitlab' size={40}></FontAwesome>
                    <Text style={styles.text5}>@Ho-oh</Text>
                    </View>
                    <View style={styles.tampilan}>
                    <FontAwesome style={styles.iconstyle2} name='github' size={40}></FontAwesome>
                    <Text style={styles.text5}>@Ho-oh</Text>
                    </View>
                    </View>
                    </View>
                <View style={styles.Box2}>
                    <Text style={styles.text4}>Hubungi Saya</Text>
                    <View style={styles.minibox2}>
                    <View style={styles.tampilan2}>
                    <AntDesign style={styles.iconstyle2} name="facebook-square" size={40}></AntDesign>
                    <Text style={styles.text5}>@Ho-oh</Text> 
                    </View>
                    <View style={styles.tampilan2}>
                    <AntDesign style={styles.iconstyle2} name="instagram" size={40}></AntDesign>
                    <Text style={styles.text5}>@Ho-oh</Text> 
                    </View>
                    <View style={styles.tampilan2}>
                    <AntDesign style={styles.iconstyle2} name="twitter" size={40}></AntDesign>
                    <Text style={styles.text5}>@Ho-oh</Text> 
                    </View>
                    </View>
                </View>
            </View>
        )
            
        
    }
}

const styles= StyleSheet.create({
    container:{
        flex:1,
        marginTop:25,
        paddingTop:30,
        backgroundColor:'white',
        marginHorizontal:13,
        justifyContent:'space-around'
    },
    page2:{
        alignItems:'center'
    },
    text1:{
        textAlign:'center',
        fontSize:36,
        color:'#003366',
        fontWeight:'700'
    },
    text2:{
        textAlign:'center',
        fontSize:24,
        color:'#003366',
        fontWeight:'700'
    },
    text3:{
        textAlign:'center',
        fontSize:16,
        color:'#3EC6FF',
        fontWeight:'700'
    },
    text4:{
        textAlign:'left',
        fontSize:16,
        color:'#003366',
        fontWeight:'400',
 
    },
    text5:{
        textAlign:'left',
        fontSize:16,
        color:'#003366',
        fontWeight:'700',
        paddingTop: 10
    },
    iconstyle:{
        color:'#EFEFEF'
    },
    iconstyle2:{
        color:'#3EC6FF',
        width:60,
        paddingLeft:10
    },
    Box1:{
        height:140,
        width:365,
        backgroundColor:'#EFEFEF',
        borderRadius:16,
        alignItems:'flex-start'
    },
    Box2:{
        height:245,
        width:365,
        backgroundColor:'#EFEFEF',
        borderRadius:16,
        alignItems:'flex-start',
    },
    minibox:{
        height:100,
        width:355,
        borderTopWidth:1,
        borderColor:'#003366',
        marginLeft:5,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-around'
    },
    minibox2:{
        height:200,
        width:355,
        borderTopWidth:1,
        borderColor:'#003366',
        marginLeft:5,
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'space-around'
    },
    tampilan2:{
        flexDirection:'row'
        
    },
    tampilan:{
        alignItems:'center',
        justifyContent:'center',
        
        
    }
})