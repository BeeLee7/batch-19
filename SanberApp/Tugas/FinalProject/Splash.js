import React, { useEffect } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
const Splash = ({navigation}) => {
    useEffect(()=>{
        setTimeout(()=>{
            navigation.navigate("Home")
        }, 3000)
    }, [])

    return (
        <View style={{justifyContent:'center', alignItems:'center', flex:1}}>
           <Image
        style={styles.tinyLogo}
        source={reqiure('./images/logoarisan.png')}
      />
            <Text style={{fontSize: 30}}>Splash Screen</Text>
        </View>
    )
}

export default Splash

const styles = StyleSheet.create({
    tinyLogo: {
        width: 50,
        height: 50,
      },
})
