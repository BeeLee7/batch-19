import React from 'react';
import {StyleSheet,
Text,
View,
TextInput,
ImageBackground,
TouchableOpacity,
ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

const ScreenContainer = ({ children }) => (
    <View style={styles.container}>{children}</View>
  );


  
export const Register= ()=>{
    
 
  

        return(
            <ScrollView>
            <ImageBackground 
            source={{uri:'https://img.freepik.com/free-vector/vibrant-pink-watercolor-painting-background_53876-58931.jpg?size=626&ext=jpg'}} 
            style={styles.background}>
               <View style={styles.container}>
                   <View style={styles.header}>
                       <TouchableOpacity>
                       <Icon style={styles.backicon} name='back' size={50}/>
                       </TouchableOpacity>
                       <Text style={styles.text1}>Hi,Kak!</Text>
                       <Text style={styles.text2}>Silahkan isi form,</Text>
                   </View>
               </View>
               <View style={styles.formcontainer}>
                   <View style={styles.form}>
                    <Text style={styles.text3} >Nama:</Text> 
                    <TextInput style={styles.textinput} placeholder='                                  '>
                    </TextInput>
                    <Text style={styles.text3} >Alamat:</Text> 
                    <TextInput style={styles.textinput} placeholder='                                  '>
                    </TextInput>
                    <Text style={styles.text3} >No.HP:</Text> 
                    <TextInput style={styles.textinput} placeholder='                                  '>
                    </TextInput>
                    <Text style={styles.text3} >Nomor kenalan yang dapat dihubungi:</Text> 
                    <TextInput style={styles.textinput} placeholder='                                  '>
                    </TextInput>
                    <Text style={styles.text3} >Username:</Text> 
                    <TextInput 
                    style={styles.textinput} 
                    placeholder='                                  '
                    onChangeText={(value)=>this.setState({username : value})}>
                    </TextInput>
                    <Text style={styles.text3} >Password:</Text> 
                    <TextInput 
                    style={styles.textinput} 
                    placeholder='                                  '
                    onChangeText={(value)=>this.setState({password : value})}>
                    </TextInput>
                    <Text style={styles.text3} >Re-type Password:</Text> 
                    <TextInput 
                    style={styles.textinput} 
                    placeholder='                                  '>
                    </TextInput>
                    <Text style={styles.text3} >KTP:</Text> 
                    <TouchableOpacity>
                        <View style={styles.filebutton}>
                        <Text style={styles.text4}>Upload File</Text>
                        </View>
                    </TouchableOpacity>
                    <Text style={styles.text3} >Foto:</Text> 
                    <TouchableOpacity>
                        <View style={styles.filebutton}>
                        <Text style={styles.text4}>Upload File</Text>
                        </View>
                    </TouchableOpacity>
                    <View style={styles.submitcontainer}>
                   <TouchableOpacity>
                        <View style={styles.submitbutton}>
                        <Text style={styles.text5}>Submit</Text>
                        </View>
                    </TouchableOpacity>
                   </View>
                   </View>
               </View>
                </ImageBackground>
                </ScrollView>
        )
    }

const styles=StyleSheet.create({
    container:{
        flex:1
    },
    background:{
            flex:1,
            width:'100%',
            height:'100%'
    },
    backicon:{
        marginTop:30
    },
    header:{
        height:150,
        width:'100%',
        backgroundColor:'transparent'
    },
    text1:{
        fontSize:48,
        color:'#FFFCFC',
        textAlign:'right',
        paddingRight:15,
        fontWeight:'400'
    },
    text2:{
        fontSize:18,
        color:'#FFFCFC',
        textAlign:'left',
        paddingLeft:60,
        fontWeight:'bold'
    },
    formcontainer:{
        flex:1,
        backgroundColor:'blue',                     //
        alignItems:'center',
        paddingTop:10
    },
    form:{
        width:340,
        height:580,
        backgroundColor:'#FBF9F9',
        borderRadius:25,
        justifyContent:'space-around'
    },
    text3:{
        color:'black',
        fontSize:12,
        marginLeft:10
    },
    textinput:{
        borderBottomWidth:1,
        width: 280,
        paddingLeft:20,
        marginLeft:35
    },
    filebutton:{
        borderWidth:0.2,
        backgroundColor:'#EFEFEF',
        height:30,
        width:80,
        marginLeft:40,
        alignItems:'center',
        justifyContent:'center'
    },
    text4:{
        fontSize:12,
        color:'#FFA31A'
    },
    submitbutton:{
        borderWidth:0.2,
        backgroundColor:'#FFA31A',
        height:30,
        width:80,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:35
    },
    text5:{
        fontSize:12,
        color:'#FFFFFF'
    },
    submitcontainer:{
        height:80,
        alignItems:'center',
        justifyContent:'center'
    }
    

})