import React from 'react';
import{View,Text,Image,StyleSheet} from 'react-native';

export default class Logo extends React.Component{
    render(){
        return(
            <View style={styles.logocontainer}>
                <Image 
                source={require('./images/logo.png')}
                style={styles.logo}/>
            </View>
        )
    }
}
const styles=StyleSheet.create({
    logocontainer:{
        marginTop:200,
        alignItems:'center',
        justifyContent:'center',
        width:330,
        height:330,
    },
    logo:{
        width:'100%',
        height:'100%',
    }
})