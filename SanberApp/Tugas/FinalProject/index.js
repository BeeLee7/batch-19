import React from "react";
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Login from'./LoginScreen';
import Register from'./RegisterScreen';

const LoginScreen = createStackNavigator();
export default class Navigation extends Component{
    render(){
        return(
    <NavigationContainer>
        <LoginScreen.Navigator>
            <LoginScreen.Screen/>
        </LoginScreen.Navigator>
        <Register.Navigator>
            <Register.Screen/>
        </Register.Navigator>
    </NavigationContainer>
       )
    }
}