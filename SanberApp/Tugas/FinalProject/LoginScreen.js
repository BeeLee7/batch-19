import React from 'react';
import {StyleSheet,
    Text,
    ImageBackground,
    View,
    Button,
    TouchableOpacity,
    TextInput
} from 'react-native';
import Logo from './Logo';

export default class Login extends React.Component {

    constructor(props){
        super(props)
        this.state={
            userName: '',
            passWord:'',
            isError: false,
        }
    }
    
    
  loginHandler() {
    console.log(this.state.userName, ' ', this.state.password)
   

    if (this.state.passWord == '123') {

      this.setState({ isError: false });
      this.props.navigation.navigate('Home', {
        userName : this.props.username
      })

    } else {

      this.setState({ isError: true });
    }

  }

render(){
    return(

        <ImageBackground 
        source={{uri:'https://img.freepik.com/free-vector/vibrant-pink-watercolor-painting-background_53876-58931.jpg?size=626&ext=jpg'}} 
        style={styles.background}>
        <View style={styles.container}>
        <View style={styles.top}>
            <Logo />
        </View>
        <View style={styles.bottom}>
         <View style={styles.loginbox}>
             <TouchableOpacity>
             <View style={styles.buttonregister}> 
                 <Text style={styles.text2} >Register</Text>
             </View>
             </TouchableOpacity>
             <Text style={styles.text1}>Or</Text>
             <TouchableOpacity>
             <View>
          <View style={styles.buttonlogin} onPress={() => this.loginHandler()}>
            <Text style={styles.text1}>Login</Text>
          </View>
        </View>
             </TouchableOpacity>
             </View>   
        </View>
        <View style={styles.typecontainer}>
                    <Text style={styles.text3} >Username:</Text> 
                    <TextInput
                    style={styles.textinput}
                    placeholder='                                  '
                    onChangeText={userName => this.setState({ userName })}>
                    </TextInput>
                    <Text style={styles.text3} >Password:</Text> 
                    <TextInput 
                    style={styles.textinput} 
                    placeholder='                                  '
                    onChangeText={passWord => this.setState({ passWord })}
                    secureTextEntry={true}>
                    </TextInput>
                    <Text style={this.state.isError ? styles.errorText : styles.hiddenErrorText }>Password Salah</Text>
            </View>   
        </View>
    </ImageBackground>
        
    )
}
}

const styles=StyleSheet.create({
    container:{
        flex:1,
   

    },
    background:{
        flex:1,
        width:'100%',
        height:'100%'
    },
    image:{
        width:100,
        height:100,
        alignItems:'center',
        justifyContent:'center'
        
    },
    top:{
        backgroundColor:'transparent',
        height:300,
        alignItems:'center',
        justifyContent:'center'
    },
    bottom:{
        height:280,
        alignItems:'center',
        justifyContent:'center',
        flexDirection:'row',
        // backgroundColor:'blue'
    },
    loginbox:{
        marginTop:50,
        height:173,
        width:321,
        borderRadius:25,
        alignItems:'center',
        backgroundColor:'#FBF9F9',
        paddingTop:10,
        paddingBottom:10,
        justifyContent:'space-around'
    },
    text1:{
        color:'#FFA31A',
        fontSize:13
    },
    buttonregister:{
        height:39,
        width:131,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:35,
        backgroundColor:'#FFA31A',
        borderWidth:0.2,
        borderColor:'black'
    },text2:{
        color:'#FFFFFF',
        fontSize:13
    },
    buttonlogin:{
        height:39,
        width:131,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:35,
        backgroundColor:'#FFFFFF',
        borderWidth:0.2,
        borderColor:'black'
    },
    typecontainer:{
        height:130,
        width:'100%',
        justifyContent:'center',
        marginLeft:35
    },
     textinput:{
        borderBottomWidth:1,
        width: 250,
        paddingLeft:20,
        marginLeft:35
    },
  errorText: {
    color: 'red',
    textAlign: 'center',
 
  },
  hiddenErrorText: {
    color: 'transparent',
    textAlign: 'center',

  }
})