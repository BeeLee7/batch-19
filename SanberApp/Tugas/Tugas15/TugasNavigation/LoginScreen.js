import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    Button,
}
from 'react-native';

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center"
    },
    button: {
      paddingHorizontal: 20,
      paddingVertical: 10,
      marginVertical: 10,
      borderRadius: 5
    }
  });
  const ScreenContainer = ({ children }) => (
    <View style={styles.container}>{children}</View>
  );

  export const Login = ({navigation}) => {
      return (
      <ScreenContainer>
          <Text>Login Screen</Text>
          <Button title= 'Menuju Skill Screen' onPress={()=> {
              this.props.navigation.navigate('Tabs')}}/>
      </ScreenContainer>
  )
  }

  export const Skill = ()=>(
      <ScreenContainer>
          <Text>Skill Screen</Text>
      </ScreenContainer>
  )
  export const Project = ()=>(
    <ScreenContainer>
        <Text>Project Screen</Text>
    </ScreenContainer>
)
export const Add = ()=>(
    <ScreenContainer>
        <Text>AddScreen</Text>
    </ScreenContainer>
)
