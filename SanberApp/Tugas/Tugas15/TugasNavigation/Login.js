import React from 'react'
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer'
import {Skill, Project,Add} from './LoginScreen';
import {About} from './AboutScreen'

const TabsStack = createBottomTabNavigator()
const SkillStack = createStackNavigator()
const ProjectStack = createStackNavigator()
const AddStack = createStackNavigator()

const SkillStackScreen = ()=> (
    <SkillStack.Navigator>
        <SkillStack.Screen name ='LoginScreen'component={Skill} title='Login Screen' />
    </SkillStack.Navigator>
)
const ProjectStackScreen= ()=>(
    <ProjectStack.Navigator>
        <ProjectStack.Screen name = 'Project' component = {Project} title='Project Screen'/>
    </ProjectStack.Navigator>   
)
const AddStackScreen= ()=>(
    <AddStack.Navigator>
        <AddStack.Screen name = 'Add' component = {Add} title='Add Screen'/>
    </AddStack.Navigator>
)
 const Tabs= () => (
    <TabsStack.Navigator>
    <TabsStack.Screen name ='SkillScreen'component={SkillStackScreen} title='Skill Screen'/>
    <TabsStack.Screen name ='ProjectScreen'component={ProjectStackScreen} title='Project Screen'/>
    <TabsStack.Screen name ='AddStackScreen'component={AddStackScreen} title='Add Screen'/>
</TabsStack.Navigator>
)
const Drawer = createDrawerNavigator();
export default ()=>(
    <NavigationContainer>
        <Drawer.Navigator>
            <Drawer.Screen name='Home' component={Tabs}/>
            <Drawer.Screen name='About' component={About}/> 
        </Drawer.Navigator>
    </NavigationContainer>   
)