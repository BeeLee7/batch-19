import React from 'react'
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {Login} from './LoginScreen';

const AuthStack = createStackNavigator();

export default ()=>(
<NavigationContainer>
    <AuthStack.Navigator>
        <AuthStack.Screen name='Login' component={Login} />
    </AuthStack.Navigator>
</NavigationContainer>
)