
// INPUT //
var nama = 'John'
var peran = 'Penyihir'
var hari = 4                // assign nilai tanggal disini
var bulan = 11              // assign nilai bulan disini
var tahun = 2020            // assign nilai tahun disini


//====================================================================//

// Tanggal //
if (
    hari<=31 && hari>=1) {
        console.log(hari)
        }
        else {
            console.log('Tanggal salah!')
        } 
        
// Bulan //
switch(bulan) {
            case 1: {
                console.log('Januari'); break;
            }
            case 2: {
                console.log('Februari'); break;
            }
            case 3: {
                console.log('Maret'); break;
            }
            case 4: {
                console.log('April'); break;
            }
            case 5: {
                console.log('Mei'); break;
            }
            case 6: {
                console.log('Juni'); break;
            }
            case 7: {
                console.log('Juli'); break;
            }
            case 8:{
                console.log('Agustus'); break;
            }
            case 9:{
                console.log('September'); break;
            }
            case 10: {
                console.log('Oktober'); break;
            }
            case 11: {
                console.log('November'); break;
            }
            case 12: {
                console.log('Desember'); break;
            }
        } 
        
// Tahun //        
if (
            tahun>=1900 && tahun<=2020) {
                console.log(tahun)
            }
            else {
                console.log('Tahun salah!')
            }

// KESALAHAN INPUT //
if (
    nama == '' && peran =='' ) {
        console.log('PERHATIAN! Nama dan Peran harus diisi')
    }
    else if (
        nama == '') {
            console.log('PERHATIAN! Nama harus diisi')
        }
        else if (
            peran == '') {
                console.log('Hai ' + nama + ' Silahkan pilih peran untuk memulai game')
            }

// INPUT SESUAI //
if (
    peran == 'Guard') {
        console.log('Selamat datan di Dunia Werewolf ' + nama + ', Hallo Guard ' + nama + ' kamu akan membantu melindungi temanmu dari serangan werewolf.')
    }
    else if (
        peran == 'Werewolf') {
            console.log('Selamat datan di Dunia Werewolf ' + nama + ', Hallo Werewolf ' + nama + ' kamu akan memakan mangsa setiap malam.')
        }
    else if (
        peran == 'Penyihir') {
            console.log('Selamat datan di Dunia Werewolf ' + nama + ', Hallo Penyihir ' + nama + ' kamu dapat melihat siapa yang jadi werewolf.')
        }
    else if (
        peran == '') {
            console.log()
        }
        else {
            console.log('Peran Salah!')
        }
