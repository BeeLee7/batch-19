  console.log('-------No.1------')                  //no.1
  golden = () => {
      console.log("This is golden")
  }
  golden()
  console.log()

console.log('-----No.2-----')                    //no.2
newFunction = (firstname,lastname) =>{
    const fullname= {firstname,lastname}
    return console.log(fullname)
  }
  newFunction('William','Imoh')
  console.log()

console.log('-------No.3-----')                  //no.3
let newObject = {
   firstName: "Harry",
   lastName: "Potter Holt",
   destination: "Hogwarts React Conf",
   occupation: "Deve-wizard Avocado",
   spell: "Vimulus Renderus!!!"
  }
  const {firstName,lastName,destination,occupation,spell} = newObject
  console.log(firstName, lastName, destination, occupation)
  console.log()

console.log('------No.4-----')                   //no.4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west,...east]
console.log(combined)
console.log()

console.log('-----No.5----')                       //no.5
const planet = "earth"
const view = "glass"
const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
console.log(before)