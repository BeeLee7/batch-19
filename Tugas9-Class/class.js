class Animal{
  constructor(name){
    this.name = name
    this._legs = 4
    this.cold_blooded = false
  }
  get legs(){
    return this._legs 
  }
  set legs(amount){
    this._legs = amount
  }

}
sheep = new Animal('shaun')
sheep.legs = 4
console.log(sheep.name)
console.log(sheep.legs)
console.log(sheep.cold_blooded)
console.log()

class Other extends Animal {
  constructor(name, x){
  super (name)
  this.legs = x
}
yell(){
 return console.log('awoo')
}
jump(){
  return console.log('hop hop')
}
}
sungokong = new Other('kerasakti',2)
frog = new Other('Buduk',4)
console.log(sungokong.name)
console.log(sungokong.legs)
sungokong.yell()
console.log()
console.log(frog.name)
console.log(frog.legs)
frog.jump()