console.log("No.1")                                         // NOMOR1 
function range(firstnum=-1,lastnum=-1){
    var angka = []
    if(firstnum>lastnum){
       var range = firstnum-lastnum+1
       for(var i = 0; i<range;i++){
       angka.push(firstnum-i)}
       return angka
    }
    else {
        var range = lastnum - firstnum+1
        for(var i=0;i<range;i++)
        angka.push(firstnum+i)
        return angka
    }
}
console.log(range())
console.log()
console.log("No.2")                                       // NOMOR 2
function rangeWithStep(firstnum,lastnum,step){
    var number = []
    if(firstnum>lastnum){
        var rentang = firstnum-lastnum+1
        for(var i= 0;i<rentang;i += step){
        number.push(firstnum - i)}
        return number
    }
    else{
        var rentang = lastnum-firstnum+1
        for(var i=0;i<rentang;i+=step){
            number.push(firstnum+i)}
        return number
    }
}
console.log(rangeWithStep(12,2,3))
console.log()
console.log("No.3")                                     // NOMOR 3
function sum(firstnum,lastnum,step=1){
    var number = []
    if(firstnum>lastnum){
        var rentang = firstnum-lastnum+1
        for(var i= 0;i<rentang;i += step){    
        number.push(firstnum - i)}
    
    }
    else{
        var rentang = lastnum-firstnum+1    
        for(var i=0;i<rentang;i+=step){
            number.push(firstnum+i)}
        
    }
    var jumlah=0
    for(var i=0;i<number.length;i++){   
        jumlah+=number[i]
    }
    return jumlah                   
}
console.log(sum(10,2))            
console.log()
console.log("No.4")                                     // nomor 4
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
    function datahandling(input){
        for(var i=0;i<4;i++){

            var ID='Nomor ID: '   + input[i][0]
            var Nama = 'Nama Lengkap: ' + input[i][1]
            var TTL = 'Tempat Tanggal lahir: ' + input[i][2] 
            var hobi = 'Hobby: ' + input[i][4]
                console.log(ID)
                console.log(Nama)
                console.log(TTL)
                console.log(hobi)
                console.log('\n')
        }
    }
    datahandling(input)
console.log()
console.log('No.5')                                                            // nomor 5
function balikkata(kata){
    var katabaru=""
    for(var i= kata.length-1;i>=0;i--){
        katabaru+=kata[i]
    }
    return katabaru
}
console.log(balikkata('sanbercode'))
console.log()
console.log('No.6')
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]  
dataHandling2(input);

function dataHandling2(data){
    var databaru = data
    var tambahnama = data[1] + " Elsharawy"
    var tambahprovinsi ="Provinsi " + data[2]
    var gender ="Pria"
    var institusi = "SMA International Metro"

    databaru.splice(1, 1, tambahnama)
    databaru.splice(2, 1, tambahprovinsi)
    databaru.splice(4, 1, gender, institusi)

    var tanggal = data [3]
    var tanggalbaru = tanggal.split('/')
    var bulan = tanggalbaru[1]
    var namabulan=" "

    switch( bulan){
    case "01":
        namabulan ="Januari"
        break;
    case "02":
        namabulan ="Februari"
        break;
    case "03":
        namabulan ="Maret"
        break;
    case "04":
        namabulan ="April"
        break;
    case "05":
        namabulan ="Mei"
        break;
    case "06":
        namabulan ="Juni"
        break;
    case "07":
        namabulan ="Juli"
        break;
    case "08":
        namabulan ="Agustus"
        break;
    case "09":
        namabulan ="September"
        break;
    case "10":
        namabulan ="Oktober"
        break;
    case "11":
        namabulan ="November"
        break;
    case "12":
        namabulan ="Desember"
        break;
    default:
        break;
    }
    
    var tanggaldijoin = tanggalbaru.join("-")    
    var namadislice= tambahnama.slice(0, 15)
    var tanggaldisort = tanggalbaru.sort(function(a, b){
        return b - a
    })

console.log(databaru)
console.log(namabulan)
console.log(tanggaldisort)
console.log(tanggaldijoin)
console.log(namadislice)
}
